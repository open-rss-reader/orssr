.DEFAULT_GOAL := all

all: daemon

daemon:
	go build -v -o ./bin/orssrd ./src

clean:
	go clean

default: daemon

.PHONY: all clean
