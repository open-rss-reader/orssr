CREATE TABLE subscription (
   id INTEGER PRIMARY KEY NOT NULL,
   title TEXT NOT NULL,
   url TEXT NOT NULL,
   last_checked_at INTEGER,
   created_at INTEGER NOT NULL
) ;

