package main

// -- FeedEntry

type FeedEntry struct {
	ID          int    `json:"id"`
	FeedID      int    `json:"feed_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Content     string `json:"content"`
	PublishedAt int64  `json:"published_at"`
}

func (db *DB) CreateFeedEntry(feedEntry *FeedEntry) (*FeedEntry, error) {
	statement, err := db.db.Prepare(`
		INSERT INTO feed_entry (feed_id, title, description, content, published_at)
			VALUES (?, ?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	res, err := statement.Exec(
		feedEntry.FeedID,
		feedEntry.Title,
		feedEntry.Description,
		feedEntry.Content,
		feedEntry.PublishedAt,
	)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()

	feedEntry.ID = int(id)

	return feedEntry, nil
}

func (db *DB) UpdateFeedEntry(feedEntry *FeedEntry) (*FeedEntry, error) {
	statement, err := db.db.Prepare(`
		UPDATE feed_entry
			SET
				feed_id=?,
				title=?,
				description=?,
				content=?,
				published_at=?
			WHERE id=? ;
	`)
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec(
		feedEntry.FeedID,
		feedEntry.Title,
		feedEntry.Description,
		feedEntry.Content,
		feedEntry.PublishedAt,
		feedEntry.ID,
	)
	if err != nil {
		return nil, err
	}

	return feedEntry, nil
}

func (db *DB) GetFeedEntriesByFeedID(id int) ([]*FeedEntry, error) {
	rows, err := db.db.Query(`
		SELECT id, feed_id, title, description, content, published_at
		FROM feed_entry
		WHERE feed_id = ?
	`, id)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	feedEntries := []*FeedEntry{}

	for rows.Next() {
		var feedEntry FeedEntry
		err := rows.Scan(
			&feedEntry.ID,
			&feedEntry.FeedID,
			&feedEntry.Title,
			&feedEntry.Description,
			&feedEntry.Content,
			&feedEntry.PublishedAt,
		)

		if err != nil {
			return nil, err
		}

		feedEntries = append(
			feedEntries,
			&feedEntry,
		)
	}

	return feedEntries, nil
}
