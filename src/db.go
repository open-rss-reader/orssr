package main

import (
	"database/sql"

	"log"

	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	db *sql.DB
}

func NewDB(dbFile string) (*DB, error) {
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	tablesStmt := `
		CREATE TABLE IF NOT EXISTS feed (
			id INTEGER PRIMARY KEY NOT NULL,
			title TEXT NOT NULL,
			url TEXT NOT NULL,
			image_url TEXT NULL,
			last_checked_at INTEGER,
			added_at INTEGER NOT NULL
		) ;

		CREATE TABLE IF NOT EXISTS feed_entry (
			id INTEGER PRIMARY KEY NOT NULL,
			feed_id INTEGER NOT NULL,
			title TEXT,
			description TEXT,
			content TEXT NOT NULL,
			published_at INTEGER NOT NULL,
			CONSTRAINT feed_entry_feed_FK FOREIGN KEY (feed_id) REFERENCES feed(id)
		) ;
	`

	_, err = db.Exec(tablesStmt)
	if err != nil {
		return nil, err
	}

	return &DB{db: db}, nil
}
