package main

import (
	"github.com/gin-gonic/gin"
	"github.com/mmcdole/gofeed"
)

type Context struct {
	DB         *DB
	FeedParser *gofeed.Parser
}

var Ctx Context

func main() {
	dbFile := "./data.db"

	db, err := NewDB(dbFile)
	if err != nil {
		panic(err)
	}

	Ctx.DB = db
	Ctx.FeedParser = gofeed.NewParser()

	r := gin.Default()

	r.Use(CORSMiddleware())

	feeds := r.Group("/feeds")
	{
		feeds.POST("/", CreateFeed)
		feeds.GET("/", GetAllFeeds)
		feeds.GET("/sync", SyncFeeds)
	}

	r.Run()
}
