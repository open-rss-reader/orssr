package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func SyncFeeds(c *gin.Context) {
	feeds, err := Ctx.DB.GetAllFeeds()
	if err != nil {
		handleError(c, err)
	}

	for _, sub := range feeds {
		feed, _ := Ctx.FeedParser.ParseURL(sub.URL)
		fmt.Println(feed.Title)

		sub.LastCheckedAt = time.Now().Unix()
		Ctx.DB.UpdateFeed(sub)
	}

	c.JSON(200, gin.H{
		"feeds": feeds,
	})
}

func CreateFeed(c *gin.Context) {
	feed := &Feed{}

	err := c.BindJSON(&feed)
	if err != nil {
		handleError(c, err)
	}

	parsedFeed, err := Ctx.FeedParser.ParseURL(feed.URL)
	if err != nil {
		handleError(c, err)
	}

	feed.Title = parsedFeed.Title
	feed.ImageURL = parsedFeed.Image.URL

	feed, err = Ctx.DB.CreateFeed(feed)
	if err != nil {
		handleError(c, err)
	}

	feed.FeedEntries = []*FeedEntry{}

	for _, entry := range parsedFeed.Items {
		feedEntry := &FeedEntry{
			FeedID:      feed.ID,
			Title:       entry.Title,
			Description: entry.Description,
			Content:     entry.Content,
			PublishedAt: entry.PublishedParsed.Unix(),
		}

		feedEntry, err := Ctx.DB.CreateFeedEntry(feedEntry)
		if err != nil {
			handleError(c, err)
		}

		feed.FeedEntries = append(
			feed.FeedEntries,
			feedEntry,
		)
	}

	c.JSON(200, feed)
}

func GetAllFeeds(c *gin.Context) {
	feeds, err := Ctx.DB.GetAllFeeds()
	if err != nil {
		handleError(c, err)
	}

	fmt.Println(feeds)

	c.JSON(200, feeds)
}

// ---------------------
// -- Gin utility functions

func handleOK(c *gin.Context) {
	c.JSON(200, gin.H{
		"status": "OK",
	})
}

func handleError(c *gin.Context, err error) {
	fmt.Println(err)

	c.JSON(500, gin.H{
		"status": "Failure",
		"error":  err,
	})
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:8000")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
