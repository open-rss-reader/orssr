package main

import (
	"time"
)

// ---------------------
// -- Models

// -- Feed

type Feed struct {
	ID            int    `json:"id"`
	Title         string `json:"title"`
	URL           string `json:"url"`
	ImageURL      string `json:"image_url"`
	LastCheckedAt int64  `json:"last_checked_at"`
	AddedAt       int64  `json:"added_at"`

	FeedEntries []*FeedEntry `json:"feed_entries"`
}

func (db *DB) CreateFeed(feed *Feed) (*Feed, error) {
	statement, err := db.db.Prepare(`
		INSERT INTO feed (title, url, image_url, last_checked_at, added_at)
			VALUES (?, ?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	res, err := statement.Exec(
		feed.Title,
		feed.URL,
		feed.ImageURL,
		time.Now().Unix(),
		time.Now().Unix(),
	)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()

	feed.ID = int(id)

	return feed, nil
}

func (db *DB) UpdateFeed(feed *Feed) (*Feed, error) {
	statement, err := db.db.Prepare(`
		UPDATE feed
			SET
				title=?,
				url=?,
				image_url=?,
				last_checked_at=?,
				added_at=?
			WHERE id=? ;
	`)
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec(
		feed.Title,
		feed.URL,
		feed.ImageURL,
		feed.LastCheckedAt,
		feed.AddedAt,
		feed.ID,
	)
	if err != nil {
		return nil, err
	}

	return feed, nil
}

func (db *DB) GetFeedByID(id int) (*Feed, error) {
	row := db.db.QueryRow(`
		SELECT id, title, url, image_url, last_checked_at, added_at
		FROM feed
		WHERE id = ?
	`, id)

	var feed Feed

	err := row.Scan(
		&feed.ID,
		&feed.Title,
		&feed.URL,
		&feed.ImageURL,
		&feed.LastCheckedAt,
		&feed.AddedAt,
	)

	if err != nil {
		return nil, err
	}

	feedEntries, err := db.GetFeedEntriesByFeedID(id)
	if err != nil {
		return nil, err
	}

	feed.FeedEntries = feedEntries

	return &feed, nil
}

func (db *DB) GetAllFeeds() ([]*Feed, error) {
	feeds := []*Feed{}

	rows, err := db.db.Query(`
		SELECT id, title, url, image_url, last_checked_at, added_at
		FROM feed
	`)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var feed Feed
		err := rows.Scan(
			&feed.ID,
			&feed.Title,
			&feed.URL,
			&feed.ImageURL,
			&feed.LastCheckedAt,
			&feed.AddedAt,
		)
		if err != nil {
			return nil, err
		}

		feedEntries, err := db.GetFeedEntriesByFeedID(feed.ID)
		if err != nil {
			return nil, err
		}

		feed.FeedEntries = feedEntries

		feeds = append(
			feeds,
			&feed,
		)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return feeds, nil
}
